package sk.ite.got.reservation.application.dto;

import java.util.Date;

public class DTOResPeriod {
	public Long id;
	public Long castleId;
	public Date dateFrom;
	public Date dateTo;
	public float priceForVisitor;
	public int numberOfVisitors;
	public int maxNumberOfVisitors;
	public Date canceledAt;
	public Date createdAt;
	public boolean finished;
	public String program;
}
