package sk.ite.got.reservation.application.dto;

public class DTODiscountCard {
	public Long id;
	public String name;
	public Integer discountInPercentage;
}
