package sk.ite.got.reservation.application.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sk.ite.got.reservation.application.dto.DTOReservation;
import sk.ite.got.reservation.application.service.ReservationService;

import java.util.List;

/**
 * Created by macalaki on 31.01.2017.
 */
@RestController
public class ReservationRestController {
    final Logger LOGGER = LoggerFactory.getLogger(ReservationRestController.class);

    @Autowired
    ReservationService reservationService;

    @RequestMapping(produces={"application/json"}, value="/reservation", method= RequestMethod.GET )
    public List<DTOReservation> getAllReservationsForCustomer(Long customerId) {
        //TODO implementation
        LOGGER.info("Getting all Reservations for customer ...");
        return reservationService.getAllReservationsForCustomer(customerId);
    }
}
