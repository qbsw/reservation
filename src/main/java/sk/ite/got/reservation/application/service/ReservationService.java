package sk.ite.got.reservation.application.service;

import sk.ite.got.reservation.application.dto.DTOReservation;

import java.util.Date;
import java.util.List;

public interface ReservationService {
	Long createReservationPeriod(Long castleId, Date from, Date to, float priceForVisitor, int maxNumberOfVisitors);
	Long createReservation(Long customerId, Long periodId, String note);
	void addGuestToReservation(Long bookingId, String name, String surname, Long discountCardId);
	void cancelReservation(long bookingId);
    List<DTOReservation> getAllReservationsForCustomer(Long customerId);

}
