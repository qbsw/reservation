package sk.ite.got.reservation;

import org.springframework.cloud.stream.annotation.EnableBinding;
import sk.ite.got.reservation.infrastructure.messaging.SubscriberCastleInterface;

@EnableBinding(SubscriberCastleInterface.class)
public class SubscriberConfig {
}
