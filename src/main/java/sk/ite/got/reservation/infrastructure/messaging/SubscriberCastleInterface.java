package sk.ite.got.reservation.infrastructure.messaging;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface SubscriberCastleInterface {
	String CASTLE_SINK = "castle-sink";

	@Input(CASTLE_SINK)
	SubscribableChannel receiveCastleEvent();
}
