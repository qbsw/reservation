package sk.ite.got.reservation.infrastructure.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sk.ite.got.reservation.domain.model.ResPeriod;

@Repository
public interface ResPeriodRepository extends JpaRepository<ResPeriod,Long> {
}
