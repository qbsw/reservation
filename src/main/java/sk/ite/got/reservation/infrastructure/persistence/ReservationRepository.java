package sk.ite.got.reservation.infrastructure.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sk.ite.got.reservation.domain.model.Reservation;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation,Long> {

}

