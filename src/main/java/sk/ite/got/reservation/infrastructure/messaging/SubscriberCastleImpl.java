package sk.ite.got.reservation.infrastructure.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Service;
import sk.ite.got.castle.domain.event.CastleCreatedEvent;

@Service
public class SubscriberCastleImpl {
	final Logger LOG = LoggerFactory.getLogger(SubscriberCastleImpl.class);
	
	@StreamListener(SubscriberCastleInterface.CASTLE_SINK)
	public void receiveCastleCreatedEvent(CastleCreatedEvent event) {
		LOG.info("Event received "+event);

	}

	
}
