package sk.ite.got.reservation.domain.model;



import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

// AggregateRoot
@Entity
public class Reservation {
	private @Id @GeneratedValue Long id;
	private @Version Long version;
	private Long customerId;
	private Long periodId;
	private String note;
	private Date lastModified;
	private Date createAt;
	private Date cancelAt;
	private Integer numberOfGuests = 0;
	private Float totalPrice = 0f;
	@ElementCollection
	@CollectionTable(
			name="GUEST",
			joinColumns=@JoinColumn(name="BOOKING_ID")
	)
	private Collection<Guest> guests = new HashSet<Guest>();

	public Reservation() {
	}

	public Reservation(Long customerId, Long periodId, String note) {
		this.customerId = customerId;
		this.periodId = periodId;
		this.note = note;

		createAt = new Date();
		lastModified = createAt;
	}

	public void addGuest(Guest guest){
		guests.add(guest);
		numberOfGuests++;
	}

	public void cancel(){
		cancelAt = new Date();
	}

	public Long getId() {
		return id;
	}
}
