package sk.ite.got.reservation.domain.model;

import javax.persistence.*;
import java.util.Date;

@Embeddable
public class Guest {
	private String name = null;
	private String surname = null;
	private Long  discountCardId;

	public Guest(String name, String surname, Long discountCardId) {
		this.name = name;
		this.surname = surname;
		this.discountCardId = discountCardId;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public Long getDiscountCardId() {
		return discountCardId;
	}
}
