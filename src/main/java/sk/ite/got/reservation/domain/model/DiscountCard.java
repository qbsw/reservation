package sk.ite.got.reservation.domain.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity
public class DiscountCard {
	private @Id @GeneratedValue Long id;
	private @Version Long version;
	private String name;
	private Integer discountInPercentage;

	public DiscountCard() {
	}

	public DiscountCard(String name, Integer discountInPercentage) {
		this.name = name;
		this.discountInPercentage = discountInPercentage;
	}

	public String getName() {
		return name;
	}

	public Integer getDiscountInPercentage() {
		return discountInPercentage;
	}
}
