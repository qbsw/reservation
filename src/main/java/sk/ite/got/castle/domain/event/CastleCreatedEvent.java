package sk.ite.got.castle.domain.event;

/**
 * Created by macalaki on 31.01.2017.
 */
public class CastleCreatedEvent {
    private Long id;
    private String name;
    private String ruler;
    private String location;

    public CastleCreatedEvent(Long id, String name, String ruler, String location) {
        this.id = id;
        this.name = name;
        this.ruler = ruler;
        this.location = location;
    }

    public CastleCreatedEvent() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getRuler() {
        return ruler;
    }

    public String getLocation() {
        return location;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRuler(String ruler) {
        this.ruler = ruler;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "CastleCreatedEvent{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", ruler='" + ruler + '\'' +
                ", location='" + location + '\'' +
                '}';
    }
}
